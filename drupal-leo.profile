<?php

/**
 * @file
 * Drupal-LEO Installatin Profile
 */
 
 /** 
 * Return a description of the profile for the profile installation screen
 * @return
 * 	An array with the keys 'name' and 'description' to describe this profile,
 * 	along with an optional 'language' to override the language selection.
 */
 
 function drupalleo_profile_details() {
 	return array(
 		'name' => 'Drupal-LEO'
 		'description' => 'Install Drupal with functionality geared towards law enforcement agencies.',
 	);
 }
 
/**
* Return an array of the modules to be enabled when this profile is installed.
*
* The following required core modules are always enabled:
* 'block', 'filter', 'node', 'system', 'user'.
*
* @return
*  An array of modules to be enabled.
*/

 function drupalleo_profile_modules() {
	return array(
	// Core Drupal modules:
	'block',
	'filter',
	'node',
	'system',
	'user',
	'dblog',
	'help',
	'menu',
	'path',
	'taxonomy',
	'color',

	// Drupal-LEO dependencies:
	'',
	'',
	);
  }
; $Id$ drupal-org.make, v 1.0-alpha 2011/04/07 17:18:00 bhosmer 

; Core version
; ------------
; This profile uses Drupal 6.x for core.

core = 6.x
projects[drupal][version] = "6.16"

; API version
; ------------
; This profile uses Drush Make API Version 2.

api = 2


; Projects
; --------
; This profile uses some contributed modules for functionality so we need to download them.


; Modules

projects[admin_menu][version] = "1.5"
projects[auto_expire][version] = "1.0-rc1"
projects[backup_migrate][version] = "2.2"
projects[calendar][version] = "2.2"
projects[ckeditor][version] = "1.1"
projects[comment_notify][version] = "1.4"
projects[cck][version] = "2.x-dev"
projects[date][version] = "2.4"
projects[features][version] = "1.0"
projects[filefield][version] = "3.3"
projects[image][version] = "1.0-beta5"
projects[imageapi][version] = "1.8"
projects[imagecache][version] = "2.0-beta10"
projects[imagefield][version] = "3.3"
projects[imce][version] = "1.4"
projects[messaging][version] = "2.2"
projects[migrate][version] = "1.0"
projects[migrate_extras][version] = "1.0-beta1"
projects[module_filter][version] = "1.6"
projects[notifications][version] = "2.2"
projects[override_node_options][version] = "1.10"
projects[pathauto][version] = "1.3"
projects[print][version] = "1.10"
projects[schema][version] = "1.7"
projects[token][version] = "1.12"
projects[tw][version] = "1.2"
projects[video][version] = "3.9"
projects[user_import][version] = "2.3"
projects[views][version] = "2.1"


